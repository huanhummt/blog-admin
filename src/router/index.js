import { createRouter, createWebHistory } from 'vue-router';

// 路由信息
const routes = [
  {
    path: '/',
    name: 'login',
    component: () => import('../views/Login/LoginView.vue')
  },
  // {
  //   path: "/register",
  //   name: "Register",
  //   component: () => import('../views/Login/RegisterView.vue'),
  // },
];

// 导出路由
const router = createRouter({
  history: createWebHistory(),
  routes
});


export default router
