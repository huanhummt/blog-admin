import { defineStore } from 'pinia'
import { ref } from 'vue'

export const useUserStore = defineStore('userStore', () => {

  // 用户信息
  const userInfoObj = ref({
    collapse: false,
    tabList: [{ name: "首页", path: "/" }],
    userId: null,
    roleList: null,
    avatar: null,
    nickname: null,
    intro: null,
    webSite: null,
    userMenuList: []
  })

  // 登陆
  const login = (userInfoObj, user) => {
    userInfoObj.userId = user.userInfoId;
    userInfoObj.roleList = user.roleList;
    userInfoObj.avatar = user.avatar;
    userInfoObj.nickname = user.nickname;
    userInfoObj.intro = user.intro;
    userInfoObj.webSite = user.webSite;
  }

  return { userInfoObj, login }
})
